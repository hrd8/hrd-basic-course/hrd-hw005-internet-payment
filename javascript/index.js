let btnStart = document.getElementById("btn-start");

let btnStop = document.getElementById("btn-stop");

let btnClear = document.getElementById("btn-clear");

let endTimeText = document.getElementById("end-time");

let startTimeText = document.getElementById("start-time");

let timeInMinutes = document.getElementById("time-in-minute");

let currentTime = document.getElementById("current-time");

let paymentInRiel = document.getElementById("payment-in-riel");

let startDays;

let savedStartDay;

let stopDays;

let totalTime;

let totalMn;

let endTimeInterval;

let isActive = true;

let hasLightClass = true;

btnClear.onclick = () => {
  clearInterval(liveTime);
  endTimeText.classList.add("text-light");
  btnClear.classList.add("d-none");
  btnStart.classList.remove("d-none");
  startTimeText.innerHTML = " 0:00";
  endTimeText.innerHTML = " 0:00";
  timeInMinutes.innerHTML = "0";
  paymentInRiel.innerHTML = "0";
  isActive = false;
};
let liveTimeFunction = () => {
  startDays = new Date();
  currentTime.innerHTML =
    startDays.toDateString() + " " + startDays.toLocaleTimeString();
};

let liveTime = setInterval(liveTimeFunction, 1000);

btnStop.onclick = () => {
  if (hasLightClass) {
    endTimeText.classList.add("text-danger");
    hasLightClass = false;
  } else {
    endTimeText.classList.remove("text-light");
    endTimeText.classList.add("text-danger");
  }
  timeInMinutes.classList.add("text-success");
  paymentInRiel.classList.add("text-success");
  btnStop.classList.add("d-none");
  btnClear.classList.remove("d-none");
  stopDays = new Date();
  totalTime = stopDays - savedStartDay;
  totalMn = Math.floor(totalTime / 60000);
  timeInMinutes.innerHTML = totalMn;
  paymentInRiel.innerHTML = calculatePrice(totalMn);
  clearInterval(endTimeInterval);
};
btnStart.onclick = () => {
  savedStartDay = new Date();
  btnStart.classList.add("d-none");
  btnStop.classList.remove("d-none");
  startTimeText.innerHTML =
    " " + startDays.toLocaleTimeString().replace(/:[\w]+ /, " ");
  endTimeInterval = setInterval(() => {
    endTimeText.innerHTML =
      " " + startDays.toLocaleTimeString().replace(/:\d+ /, " ");
  }, 1000);
  if (!isActive) {
    liveTime = setInterval(liveTimeFunction, 1000);
    isActive = true;
  }
};
let calculatePrice = (minute) => {
  let usagePrice = 500;
  let temp;
  let hourPrice;
  for (let i = 1; i < 10; i++) {
    if (minute >= 60) {
      hourPrice = Math.floor(minute / 60) * 1500;
      minute = minute % 60;
      if (minute == 0) {
        hourPrice -= 500;
      }
    } else {
      temp = Math.floor(minute / (15 * Math.pow(2, i - 1) + 0.01));
    }
    temp = Math.floor(minute / (15 * Math.pow(2, i - 1) + 0.01));
    if (temp == 0 && hourPrice > 0) {
      return usagePrice + hourPrice;
    } else if (temp == 0) {
      return usagePrice;
    }
    usagePrice += 500;
  }
};
//testing method for calculatePrice function..
// let testFunction = (minute) => {
//   console.log(minute + " Minute's price is: " + calculatePrice(minute));
// };
// testFunction(0);
// testFunction(15);
// testFunction(16);
// testFunction(30);
// testFunction(31);
// testFunction(60);
// testFunction(65);
// testFunction(76);
// testFunction(91);
// testFunction(120);
// testFunction(121);
// testFunction(180);
// testFunction(181);
